%% setup
% saveas(gcf, 'z:\\code\\expmon\\mainwin.fig');
% close(gcf);
% saveas(gcf, 'z:\\code\\expmon\\latticewin.fig');
if exist('x') && isstruct(x) && isfield(x, 'close')
  x.close();
  daqreset
  clear all
  close all
end

%% MOT, magnetic trap, and dipole beam
% Look into using plots.m on Matlab file xchg to draw several y axes
% Use spare SCB-68 to try out single ended inputs
x = expmon('nidaq','Dev1')

x.display('Primary', {'Vert', [-0.25, 6.0], 'Horz', 100, 'ScrollTime', false,...
   'Title', 'Trapping and Cooling', 'XLabel', 'time (s)', 'YLabel', 'volts'},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'},...
   'z:\\code\\expmon\\mainwin.fig') 
x.channel(1, 'MOT', 'Primary', {}, {'Color', 'w'})

% measure(display, {list of HWchans to pass}, meas name, meas fn, meas window)  
x.measure('Primary', {'MOT'}, 'Mean', @mean, -0.5)
x.measure('Primary', {'MOT'}, 'Std Dev', @std, -0.5)

% Dipole beam
x.channel(3, 'Dipole_Beam', 'Primary', {'Scale', 5}, {'Color', 'r'})
x.measure('Primary', {'Dipole_Beam'}, 'Mean (Watts)', @(x) mean(x)*12.356, -0.5)

% Magnetic trap
x.channel(4, 'Pinch_Coils', 'Primary', {'Scale', 2}, {'Color', 'g'})
x.measure('Primary', {'Pinch_Coils'}, 'Mean (Amps)', @(x) mean(x)/0.002, -0.5)

x.channel(5, 'Bias_Coils', 'Primary', {'Scale', -10}, {'Color', 'y'})
x.measure('Primary', {'Bias_Coils'}, 'Mean (Amps)', @(x) mean(x)/0.002, -0.5)

x.channel(6, 'QP_Coils', 'Primary', {'Scale', 5}, {'Color', 'b'})
x.measure('Primary', {'QP_Coils'}, 'Mean (Amps)', @(x) mean(x)/0.002, -0.5)

% AG coil
x.channel(7, 'AG_Coil', 'Primary', {'Scale', 2}, {'Color', 'c'})
x.measure('Primary', {'AG_Coil'}, 'Mean (Amps)', @(x) mean(x)/0.1, -0.5)

%% AG coil

%x.display('AGDisp', {'Vert', [-0.1, 3], 'Horz', 1, 'PointHorz', 0.001, 'ScrollTime', false,...
%   'Title', 'Anti-Gravity Coil', 'XLabel', 'time (s)', 'YLabel', 'volts'},...
%   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'}) 
 
%% Lattice Beams 
% TODO: consider matching scales of lattice beams; could offset so all are
%  visible even when depth is the same

x.display('Lattice', {'Vert', [-0.1, 2], 'Horz', 0.35, 'ScrollTime', false,...
  'Title', 'Lattice Beams', 'XLabel', 'time (s)', 'YLabel', 'volts'},...
  {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'},...
  'z:\\code\\expmon\\latticewin.fig')

x.channel(2, 'Speckle', 'Lattice', {}, {'Color', 'w'})
% Differential channel HWid is channel number of first channel in pair
x.channel(16, 'Beam_1', 'Lattice', {}, {'Color', 'r'})
x.channel(17, 'Beam_2', 'Lattice', {}, {'Color', 'g'})
x.channel(18, 'Beam_3', 'Lattice', {}, {'Color', 'y'})

%% Lattice display trigger fn (state function)
% trigger is signaled when value of trigger function changes from negative
%  to positive
% first arg to trig fn is most recent nonzero return value
LTrigStart = -1;
LTrigStop = 1;
% trigger lattice display on falling edge of lattice
 
LatticeTrigFn = @(prev, L1, L2, L3, S)...
  [ prev == LTrigStop & (L1 > 0.1 | L2 > 0.1 | L3 > 0.1 | S > 0.1),...
    prev == LTrigStart & (L1 < 0.1 & L2 < 0.1 & L3 < 0.1 & S < 0.1)...
  ] * [LTrigStart; LTrigStop];

% by default, trig fn is passed data after filtering
% trigger(trigger name, display, {channels to pass to trigger}, trigger fn,...
%   trigger delay (edge to start of acquisition) (s), min trig width (s))
%%%x.trigger('LTrig', 'Lattice', {'Beam_1', 'Beam_2', 'Beam_3', 'Speckle'},...
%%%  LatticeTrigFn, LTrigStop, -0.3, 0.01)

% Trigger on falling edge of pinch coils - using dipole trap would be 
%  better, but voltage is much smaller
PinchTrigFn = @(prev, PP)...
  [ prev == LTrigStop & (PP > 0.1),...
    prev == LTrigStart & (PP < 0.1)...
  ] * [LTrigStart; LTrigStop];

DipoleTrigFn = @(prev, DD)...
  [ prev == LTrigStop & (DD > 0.1),...
    prev == LTrigStart & (DD < 0.1)...
  ] * [LTrigStart; LTrigStop];

%x.trigger('LTrig', 'Lattice', {'Pinch_Coils'}, PinchTrigFn, LTrigStop, -0.3, 0.01)
x.trigger('LTrig', 'Lattice', {'Dipole_Beam'}, DipoleTrigFn, LTrigStop, -0.3, 0.01)

%{
LatticeTrigTest = @(prev, MOT)...
  [ prev == LTrigStop & (MOT < -0.075),...
    prev == LTrigStart & (MOT > -0.075)...
  ] * [LTrigStart; LTrigStop];

x.trigger('Lattice', {'MOT'},...
  LatticeTrigTest, LTrigStop, -0.8, 0.01)
%}

%% watch MOT -> QP transfer

x.display('Capture', {'Vert', [-0.25, 6.0], 'Horz', 0.150, 'ScrollTime', false,...
   'Title', 'QP Capture', 'XLabel', 'time (s)', 'YLabel', 'volts'},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'},...
   'z:\\code\\expmon\\capturewin.fig') 
 
x.channel(1, 'MOT2', 'Capture', {}, {'Color', 'w'})
x.channel(6, 'QP_Coils2', 'Capture', {'Scale', 5}, {'Color', 'b'})
%x.measure('Primary', {'QP_Coils'}, 'Mean (Amps)', @(x) mean(x)/0.002,-0.5)

MOTTrigFn = @(prev, PP)...
  [ prev == LTrigStop & (PP > 2),...
    prev == LTrigStart & (PP < 2)...
  ] * [LTrigStart; LTrigStop];

x.trigger('CaptureTrig', 'Capture', {'MOT2'}, MOTTrigFn, LTrigStop, -0.05, 0.01)

%% go
x.start();

%x.stop()

