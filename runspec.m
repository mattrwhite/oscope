%% spectrograms from sound card 
if exist('x') && isstruct(x) && isfield(x, 'close')
  x.close();
  daqreset
  clear all
  close all
end

%% create spectrogram window 
% defaults to sound card (winsound), 10 KSPS
x = expmon()

% subsequent calls to specgramscope() will find the window
Nfft = 2048;

x.display('Spec', {'CondType', 'Spec', 'NPoints', Nfft,...
   'Vert', [64, 4096], 'Horz', 10, 'ScrollTime', false,...
   'Title', 'Spectrogram (2048 pt FFT)', 'ShowLegend', false,...
   'XLabel', 'time (s)', 'YLabel', 'freq (Hz)'},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'off', 'YGrid', 'off', 'Color', 'k',...
   })
 
x.channel(1, 'MicSpec', 'Spec',...
  {'Scale', 1, 'Offset', 100, 'Window', hamming(Nfft)}, {})

%% log scale spectrogram

x.display('LogSpec', {'CondType', 'LogSpec', 'NPoints', Nfft,...
   'Vert', [64, 4096], 'NumYTicks', 7, 'Horz', 10, 'ScrollTime', false,...
   'Title', 'Spectrogram (Const. Q)', 'ShowLegend', false,...
   'XLabel', 'time (s)', 'YLabel', 'freq (Hz)'},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'off', 'YGrid', 'off', 'Color', 'k',...
   })
 
x.channel(1, 'MicLogSpec', 'LogSpec',...
  {'Scale', 1, 'Offset', 100, 'Window', hamming(Nfft)}, {})

%% create regular plot

x.display('Primary', {'Vert', [-1, 1], 'Horz', 10, 'ScrollTime', false, 'StopBtn', true,...
   'Title', 'Sound Test', 'XLabel', 'time (s)', 'YLabel', 'units', 'ScrollBack', 3},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'})

x.channel(1, 'Mic', 'Primary', {}, {'Color', 'w'})
 
%% go
x.start()
