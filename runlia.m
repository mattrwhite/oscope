%% data (over TCP/IP) from lockin amplifier
if exist('x') && isstruct(x) && isfield(x, 'close')
  x.close();
  clear all
  close all
end

%% ADC output
% ADC rate is 100e6 samples/sec ... but we don't get data continuously from
%  ADC!  Time axis labels will be wrong.
SPS = 5120; %100e6;
x = expmon('int16://172.25.128.168:5001', 1, SPS)
set(x.ai, 'ByteOrder', 'littleEndian');

% We will not add a trigger object, so this will be a "roll" display. If
%  we set Horz <= LIA DMA buffer length, the display won't actually roll
%  (will be like scope in Auto mode).
x.display('Primary', {'Vert', [-2.5e4, 2.5e4], 'Horz', 512/SPS, 'AutoVert', 1.1,...
   'ScrollTime', false, 'StopBtn', true, 'ScrollBack', 3, 'ForceDraw', true,...
   'Title', 'ADC output', 'XLabel', 'time (s)', 'YLabel', 'units'},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'})

x.channel(1, 'ADC', 'Primary', {}, {'Color', 'w'})

% TODO: use measurement fn to display packet number

%% filtered output
SPS = 2500; % 2.5 MSPS/1000 (downsamp factor)
x = expmon('double://172.25.128.168:5001', 2, SPS)
set(x.ai, 'ByteOrder', 'littleEndian');

x.display('Primary', {'Vert', [-1, 1], 'Horz', 10, 'AutoVert', -1.1,...
   'ScrollTime', false, 'StopBtn', true, 'ScrollBack', 3, 'ForceDraw', true,...
   'Title', 'Filtered output', 'XLabel', 'time (s)', 'YLabel', 'units'},...
   {'XColor', 'w', 'YColor', 'w', 'XGrid', 'on', 'YGrid', 'on', 'Color', 'k'})

x.channel(1, 'I', 'Primary', {}, {'Color', 'r'})
%x.channel(2, 'Q', 'Primary', {}, {'Color', 'g'})
