% Experiment Monitor object - Matt White, University of Illinois
% License: GNU GPL 2.0

% TODO: more documentation
% TODO: fix horribly hacked UpdateTrigDisp inner loop
% TODO: give measurements names to allow for on-the-fly changes
% TODO: On-the-fly changes for display - how to get around problem of
%  .channels, .haxes, etc.  Start with simple changes like Vert and Horz.
% TODO: programmable holdoff for measurements
% TODO: if trigger is inside display window, optionally draw vertical line
% TODO: optionally save all data points and swap this data set in when 
%  acquisition is stopped so user can zoom in on plot
% TODO: max trigger width?
% TODO: digital phosphor feature: instead of overwriting lineseries object,
%  create new line series object, delete oldest lineseries, and update
%  ColorSpec for remaining lineseries objects.
% TODO: FFT/math - allow measurements to output a full trace instead of
%  just a scalar
% TODO: variable Horz, defined by trigger! (not with digital phosphor)

% MORE:
% TODO: allow different CondType for each channel on display (?)
% TODO: check out TM Widgets and Stripchart on MATLAB central

% Optimization Notes:
% If a MATLAB legend is present on the plot, drawnow will call
%  legendcolorbarlayout(), which is extremely slow even if the legend
%  is unchanged.  - So we just make out own cheesy text box legend.
% For this, annotation('textbox',...) object is extremely slow; using 
%  text() is much faster
% Profiler results: data append in UpdateAll is slow (try circular buffer?);
%  CondFn and MeasFn are the slowest operations in DrawTraces

function this = expmon(adapter, hwID, SamplesPerSecond)

% MeasWindow = Inf - use all (sampled/visible) data points for measurement
chandefaults = struct(...
  ... internal data (do not set)
  'haxes', [], 'dispdata', [], 'hline', [], 'DataWidth', 1, 'Measurements', [],...
  ... set by args to channel() (do not set directly)
  'Name', '',... % Short name for channel. No spaces!
  'Display', '',... % display on which channel appears
  'HWchan', [],...   % data source (i.e hardware channel)
  ... scale, offset, filtering fn (optional)
  'Scale', 1, 'Offset', 0, 'FilterFn', [],...
  ... FFT window for spectrogram display
  'FFTWin', 1,...
  ... label for channel in legend
  'LegendStr', [],...
  ... fn used to condense multiple data points to single display point
  'CondFn', @onesidedstds...
);

% display
dispdefaults = struct(...
  ... internal data (do not set)
  'haxes', [], 'htextbox', [], 'hslider', [], 'triggers', {{}},...
  'NumHeldPts', 0, 'disptime', [], 'time', [], 'channels', {{}},...
  'TimeWidth', 1,...  % REMOVE ME?
  'NextTrigTime', 0,...  % internal; for trigger holdoff
  ... labels and ticks
  'Title', 'No Title', 'XLabel', 'Time', 'YLabel', 'Volts',...
  'YTicks', [], 'NumYTicks', 10,...  
  ... y axis limits (relative to final, scaled display data)
  'Vert', [-1, 1],...
  ... horizontal scale (i.e., x axis range), in seconds
  'Horz', 1,...
  ... show absolute time on x axis?
  'ScrollTime', true,...
  ... if >0, continuously adjust vertical scale to be AutoVert * data range
  ...  if <0, same, but grow only
  ... Ignored for spectrogram displays (obviously)
  'AutoVert', 0,...
  ... method for combining raw data to create plot points
  ... for CondFn which returns two values [a, b], we create display points at
  ...  MinMax: (time, a) and (time, b)
  ...  CenterSpan: (time, a-b) and (time, a+b)
  ... other display types:
  ...  Simple: just take every Nth data point, discarding others
  ...  Spec, LogSpec: spectrogram displays (linear or log axis) 
  'CondType', 'MinMax',...
  ... display mode: Roll is continuous, automatic updating
  ...  changed to Trig if any triggers are added to display
  'Mode', 'Roll',...
  ... create text column to right of plot for legend and measurements
  'ShowLegend', true,... 
  ... total length of viewable data, as multiple of Horz; >1 creates a 
  ...  scrollbar below plot
  'ScrollBack', 1,...
  ... show a Start/Stop button on this display - only enable for one display
  'StopBtn', false,... 
  ... call drawnow after updating traces
  'ForceDraw', false...
);

% init as empty structs
channels = struct;
displays = struct;
triggers = struct;
usedchans = [];

close all

% We'll manage analoginput/tcpip object rather than have caller do so
%  and pass it to us.  This is debateable.

% setup analog input object
if nargin == 0
  adapter = 'winsound';
end
if nargin < 3
  SamplesPerSecond = 10000;
end
% check for network data sources - only raw, pushed TCP/IP data
%  supported for now
tokens = regexp(adapter, '(\w+)://([\w\.]+):(\d+)', 'tokens');
if ~isempty(tokens)
  tcpdatatype = tokens{1}{1};
  ai = tcpip(tokens{1}{2}, str2num(tokens{1}{3}));
  
  ai.BytesAvailableFcn = @UpdateAll;
  ai.BytesAvailableFcnMode = 'byte';
  ai.InputBufferSize = SamplesPerSecond * sizeof(tcpdatatype);
  ai.BytesAvailableFcnCount = ai.InputBufferSize/10;
  % TCP data may contained interleaved multi-channel data
  %  second arg is number of channels
  usedchans = 1:hwID;
  lasttcptime = 0;
else
  if nargin > 1
    % hwID can be string or int
    ai = analoginput(adapter, hwID);
  else
    ai = analoginput(adapter);
  end
  
  % SamplesPerSecond is per channel (all channels have the same sample rate)
  set(ai, 'SampleRate', SamplesPerSecond);
  set(ai, 'SamplesPerTrigger', Inf);
  set(ai, 'SamplesAcquiredFcn', @UpdateAll);
  set(ai, 'SamplesAcquiredFcnCount', SamplesPerSecond/10);  
  %set(ai, 'TimerFcn', @UpdateAll);
  %set(ai, 'TimerPeriod', 0.1);  
end

MaxPtsPerUpdate = SamplesPerSecond/2;

function start_(hbtn)
  if nargin > 0
    % change start button to stop button
    set(hbtn, 'String', 'Stop', 'Callback', @(hobj, event) stop_(hobj));    
  end
  
  FinalizeDisplays();
  pause(1);
  switch class(ai)
  case 'analoginput'
    start(ai);
  case 'tcpip'
    fopen(ai);
  end % switch
end

% Under heavy load, code from command prompt may be severely delayed;
%  instead, a stop button can be added to one (or more) scope window
%  to call stop_() with minimal delay
function stop_(hbtn)
  if nargin > 0
    % change stop button to start button
    set(hbtn, 'String', 'Start', 'Callback', @(hobj, event) start_(hobj));    
  end
  
  switch class(ai)
  case 'analoginput'
    if isrunning(ai)  
      stop(ai);
      disp('Analog input object has been stopped');
    end
  case 'tcpip'
    fclose(ai);
    disp('TCP/IP connection closed.');
  end % switch
end

function close_()
  %set(ai, 'TimerFcn', @stop);  
  stop_();
  delete(ai);
end

% Scrolling time axis - just use a simple slider uicontrol (scrollbar)
% scrollplot.m on MATLAB file exchange creates a "preview" subplot with 
%  draggable, resizable window to set main axes view (but code is 1300+ lines) 
function doslider(dispname)
  disp = displays.(dispname);
  xlim(disp.haxes, [-disp.Horz, 0] + ...
    (1-get(disp.hslider,'value'))*disp.Horz*(1-disp.ScrollBack) + ...
    disp.disptime(end)*disp.ScrollTime);
end

function FinalizeDisplays()
  % TODO: what to do if a display has been closed?
  % can't just recreate display - have to recreate lineseries objects, etc
  % one option is to cache arguments for all calls to display(), channel(),
  %   trigger(), and measure() and repeat them everytime start is called

  % calculate lengths of .data field for each channel and initialize
  % Length = max(disp.Horz + max(0,max(disp.Triggers.Delay))
  for channame = fieldnames(channels)'
    channame = char(channame);
    if channels.(channame).DataWidth < MaxPtsPerUpdate;
      channels.(channame).DataWidth = MaxPtsPerUpdate;
    end
    samps = channels.(channame).DataWidth;
    channels.(channame).data = zeros(samps,1);
    %channels.(channame).dispdata = [];    
    if displays.(channels.(channame).Display).TimeWidth < samps
      displays.(channels.(channame).Display).TimeWidth = samps;
    end
  end
  
  % get width of each display in pixels
  for dispname = fieldnames(displays)'
    dispname = char(dispname);
    horz = displays.(dispname).Horz;
    haxes = displays.(dispname).haxes;
    % set xlim if ScrollTime is false
    if ~displays.(dispname).ScrollTime
      xlim(haxes, [-horz, 0]); 
    end
    
    % add scroll bar below axes, if requested
    if displays.(dispname).ScrollBack > 1
      % set up callback for slider (created in display_())
      % if we try to use displays.(dispname) directly in anon fn, the struct
      %  will be cached in it's current state!
      slidercb = @(hObj, eventdata) doslider(dispname); %doslider(get(hObj,'value'), dispname);
      lh1 = addlistener(displays.(dispname).hslider, 'Action', slidercb);
    end

    % obtain width of axes in pixels - 3rd element of pos
    set(haxes, 'Units', 'pixels');
    pos = get(haxes, 'Position');
    set(haxes, 'Units', 'normalized');
    displays.(dispname).time = zeros(displays.(dispname).TimeWidth,1);    
    % tskip is samples per pixel
    tskip = horz * SamplesPerSecond/pos(3);
    % for now, just adjust Horz to make tskip integer
    % TODO: option to adjust pixels, or change how tskip is used
    %  so that it doesn't have to be integer
    if floor(tskip) ~= tskip
      if tskip > 10
        % the desired effect of this is hide a few points off the left edge
        %  of the axes in order to make tskip an integer
        % TODO: might actually look better if we use ceil!
        tskip = floor(tskip);
      else
        tskip = 1;
      end
    end
    displays.(dispname).tskip = tskip;
    plotpts = round(horz * SamplesPerSecond/tskip) * displays.(dispname).ScrollBack;
    if tskip > 1 && any(strcmp(displays.(dispname).CondType, {'MinMax', 'CenterSpan'}))
      plotpts = plotpts * 2;
    end
    displays.(dispname).disptime = ...
      linspace(-horz - 1/SamplesPerSecond, -1/SamplesPerSecond, plotpts)';
    for ii = 1:length(displays.(dispname).channels)
      channame = char(displays.(dispname).channels{ii});
      switch displays.(dispname).CondType
      case 'LogSpec'
        set(channels.(channame).himage, 'YData', get(haxes, 'YLim'));          
        channels.(channame).dispdata = ...
          zeros(size(channels.(channame).CQKernel, 2), plotpts);%*NaN;          
      case 'Spec'
        %f = (0:floor(npts/2)-1)*SamplesPerSecond/npts;
        set(channels.(channame).himage, 'YData', [0, SamplesPerSecond/2]);          
        channels.(channame).dispdata = ...
          zeros(floor(displays.(dispname).NPoints/2), plotpts);%*NaN;
      otherwise
        channels.(channame).dispdata = ones(plotpts,1)*NaN;
      end
    end
    displays.(dispname).NextTrigTime = 0;
    displays.(dispname).NumHeldPts = 0;    
  end
  
  for trigname = fieldnames(triggers)'
    trigname = char(trigname);
    triggers.(trigname).NumHeldPts = 0;
  end
  
 % misc
 lasttcptime = 0;
end

% TODO: automatic sizing of axes to best fill window, or allow caller to
%  specify size
function display_(name, scopeprops, plotprops, figfile)
  displays.(name) = structunion(struct(scopeprops{:}), dispdefaults);
  %displays.(name).plotprops = plotprops;
  % adjust .Horz to be a multiple of sample period
  displays.(name).Horz = round(displays.(name).Horz * SamplesPerSecond) / SamplesPerSecond;
  htextbox = [];
  hslider = [];
  
  try
    open(figfile);
    % TODO: find and protect slider control (Type = uicontrol)
    haxes = findobj(get(gcf, 'Children'), 'Type', 'axes');
    % get the legend box - clear all other children of axes
    htextbox = findobj(get(haxes, 'Children'), 'Type', 'text');
    % protect textbox from clear axes
    set(htextbox, 'HandleVisibility', 'off');
    cla(haxes);
    set(htextbox, 'HandleVisibility', 'on');
  catch
    figure;
    haxes = axes(plotprops{:});
    
    switch displays.(name).CondType
    case 'LogSpec'
      % MATLAB thinks axis is linear, when it is actually logarithmic, so
      %  we have to manually set ticks labels and positions
      yt = displays.(name).YTicks;
      if isempty(yt)
        %yt = linspace(disp.Vert[1], disp.Vert[2], displays.(name).NumYTicks);
        yt = logspace(log10(displays.(name).Vert(1)),...
          log10(displays.(name).Vert(2)), displays.(name).NumYTicks);
      end
      % doesn't really matter if we use log, log10, log2, etc. here
      set(haxes, 'YTick', log10(yt));
      ylim(log10(displays.(name).Vert));
      % in either (log or lin) case, this is how we create labels
      ytl = sprintf('%.0f|', yt);
      set(haxes, 'YTickLabel', ytl(1:end-1));  % drop trailing '|'
    otherwise
      ylim(displays.(name).Vert);
    end
    
    xlabel(displays.(name).XLabel);
    ylabel(displays.(name).YLabel);  
    title(displays.(name).Title);  
    if displays.(name).ShowLegend
      set(haxes, 'Position', [0.1, 0.1, 0.7, 0.8]);
      htextbox = text(1.02, 1.00, 'Legend', 'Units', 'normalized',...
        'VerticalAlignment', 'top');
      if displays.(name).StopBtn
        h = uicontrol('Style', 'pushbutton', 'String', 'Start',...
         'Units', 'normalized', 'Position', [0.85 0.1 0.1 0.05],...
         'Callback', @(hobj, event) start_(hobj));
      end
    end
    if displays.(name).ScrollBack > 1
      % This will create a slider which is just underneath the axis
      % but still leaves room for the axis labels above the slider
      pos = get(haxes,'position');
      sliderpos = [pos(1) pos(2)-0.1 pos(3) 0.05];
      hslider = uicontrol('style','slider', 'units','normalized',...
        'position', sliderpos ,'min', 0, 'max', 1, 'Value', 1);
    end
  end    
  displays.(name).haxes = haxes;  
  displays.(name).htextbox = htextbox;
  displays.(name).hslider = hslider;
end

% tcpip stream may (will for LIA) consist of interleaved data - how to
%  handle?
function chan = channel_(HWchan, name, dispname, scopeprops, lineprops)
  switch class(ai)
  case 'analoginput'
    % add the channel to AI object
    chanidx = find(usedchans == HWchan);
    if isempty(chanidx)
      aichanobj = addchannel(ai, HWchan);
      chanidx = aichanobj.Index;    
    end
    usedchans(chanidx) = HWchan; 
  case 'tcpip'
    chanidx = HWchan;
  end

  chan = orderfields(structunion(struct(scopeprops{:}), chandefaults));
  chan.Name = name;
  chan.Display = dispname;
  chan.HWchan = HWchan;
  chan.ChanIdx = chanidx;
  % will be needed below...
  legendname = name;
  legendname(name == '_') = ' ';
  
  % add lineseries object to specified display
  chan.haxes = displays.(dispname).haxes;
  % set specified display as current axes for the line() command
  axes(chan.haxes);
  switch displays.(dispname).CondType
  case {'Spec', 'LogSpec'}
    if ~isempty(displays.(dispname).channels)
      error('Only one channel may be associated with a spectrogram display.');
    end
    if strcmp(displays.(dispname).CondType, 'LogSpec')
      % TODO: make num. bins a variable; move NPoints to channel object
      chan.CQKernel = sparseKernel(displays.(dispname).Vert, 16, SamplesPerSecond);
      displays.(dispname).NPoints = size(chan.CQKernel, 1);
    end
    chan.himage = image(lineprops{:});
    % surf object lets us use log freq axis, but is way too slow
    %chan.himage = surf('LineStyle', 'none', lineprops{:}); view(0,90);
    % number of FFT points
    chan.DataWidth = displays.(dispname).NPoints + MaxPtsPerUpdate;
    chan.LegendStr = ['\bf', legendname, '\rm'];    
  otherwise
    chan.hline = line(0, NaN, lineprops{:}); 
    % setup string for displaying name of channel
    % Use font that includes ASCII line drawing characters?
    rgb = mat2str(get(chan.hline, 'Color'));
    % replace underscores with spaces in legend string
    % TODO: option for user to specify legendname
    chan.LegendStr = ['\bf', legendname, ' {\color[rgb]{', rgb(2:end-1), '}--}\rm'];
  end
  % store channel info
  channels.(name) = chan;
  displays.(dispname).channels{end+1} = name;
end

function trigger_(trigname, dispname, trigchans, trigfn, initialstate, delay, minwidth)
  % wrap user's trigger function with code to extract and pass new data for
  %  specified channels (trigchans)
  trigfnargstr = cellfun(@(x) [', chans.' x '.data(end-revidx0:end-revidx1)'], trigchans, 'UniformOutput', false);
  trig.Fn = eval(['@(prev, chans, revidx0, revidx1) trigfn(prev' trigfnargstr{:} ')']);
  % delay is passed as delay to acq start in seconds, but is stored
  %  internally as delay to acq stop in samples
  trig.Delay = round((delay + displays.(dispname).Horz) * SamplesPerSecond);
  trig.State = initialstate;
  if nargin > 5
    trig.MinWidth = max(1,round(minwidth * SamplesPerSecond));
  else
    trig.MinWidth = 1;
  end
  trig.NumHeldPts = 0;
  % default hold off is delay to acq stop in seconds
  trig.Holdoff = delay + displays.(dispname).Horz;
  triggers.(trigname) = trig;
  displays.(dispname).triggers{end+1} = trigname;  
  displays.(dispname).Mode = 'Trig';
  
  % ensure that all channels retain enough data for this trigger
  if trig.Delay < 0
    reqsamps = (displays.(dispname).Horz + abs(delay)) * SamplesPerSecond;
  elseif delay > 0
    reqsamps = trig.Delay;
  else
    reqsamps = displays.(dispname).Horz * SamplesPerSecond;
  end
  % some padding is required since old data is removed as soon as new data is added
  reqsamps = reqsamps + trig.MinWidth + MaxPtsPerUpdate;
  for ii = 1:length(trigchans)
    channame = char(trigchans{ii});
    if channels.(channame).DataWidth < reqsamps
      channels.(channame).DataWidth = reqsamps;
      disp(['Trigger extends buffer length for channel ' channame]);
    end
  end
  % similarly for channels to be displayed (FIX THIS COPYPASTA)
  for ii = 1:length(displays.(dispname).channels)
    channame = char(displays.(dispname).channels{ii});
    if channels.(channame).DataWidth < reqsamps
      channels.(channame).DataWidth = reqsamps;
      disp(['Trigger extends buffer length for channel ' channame]);
    end
  end    
end

% temporarily replace trig.Fn with fn that generates a trigger
% fn should generate some number of -1's (holdoff or mintrigwidth?),
%  then mintrigwidth 1's.
% UpdateTrigDisp restores original trig.Fn?
function forcetrigger_(trigname)
  triggers.(trigname).TrigLowCount_ = ceil(triggers.(trigname).Holdoff * SamplesPerSecond + 1);
  triggers.(trigname).oldFn_ = triggers.(trigname).Fn;
  triggers.(trigname).Fn = @(varargin) forcetrigfn_(trigname, triggers.(trigname).Fn(varargin{:}));
end

function y = forcetrigfn_(trigname, pts)
  y = ones(size(pts));
  cnt = triggers.(trigname).TrigLowCount_;
  if cnt > 0
    y(1:min(length(y), cnt)) = -1;
    triggers.(trigname).TrigLowCount_ = cnt - length(pts);
  end
end

% TODO: should measwindow be relative to display instead of trigger?
% TODO: allow user to pass initial value for .prev?

% if useprev == true, first arg to measfn will be its last return value
function measure_(display, measchans, measname, measfn, measwindow, useprev) 
  
  if nargin < 5 || isempty(measwindow)
    measwindow = [0.1, 0];
  elseif length(measwindow) == 1
    % measwindow should be < 0 in this case (enforce?)
    measwindow = [measwindow, 0];
  end
  
  samps = measwindow*SamplesPerSecond;
  argstr = cellfun(@(x) [', chans.' x '.data(end-trigidx+samps(1)+1:end-trigidx+samps(2))'], measchans, 'UniformOutput', false);
  argstr = [argstr{:}];
  if nargin < 6 || ~useprev
    meas.Fn = eval(['@(prev, chans, trigidx) measfn(' argstr(3:end) ')']);
  else
    meas.Fn = eval(['@(prev, chans, trigidx) measfn(prev' argstr ')']);
  end    
  meas.Window = measwindow;
  meas.Name = measname;
  meas.prev = 0;
  % measurement is displayed with first channel listed in measchans
  if length(channels.(char(measchans{1})).Measurements) > 0
    channels.(char(measchans{1})).Measurements(end+1) = meas;
  else
    channels.(char(measchans{1})).Measurements = meas;
  end

  % ensure that all channels retain enough data for this measurement
  % TODO: fix this - might need to require all triggers are created
  %  before any measurements  
  reqsamps = -samps(1) + SamplesPerSecond/4;  
  for ii = 1:length(measchans)
    channame = char(measchans{ii});
    if channels.(channame).DataWidth < reqsamps
      channels.(channame).DataWidth = reqsamps;
      disp(['Measurement extends buffer length for channel ' channame]);
    end
  end  
end

% TODO: consider reversing direction of chan.data, disp.time, so we can index
%  from beginning instead of end.

% TimerFcn/SamplesAcquiredFcn callback - update all displays
function UpdateAll(obj, event)
  MaxNumHeldPts = 0;
  for dispname = fieldnames(displays)'
    MaxNumHeldPts = max(MaxNumHeldPts, displays.(char(dispname)).NumHeldPts);    
  end
  
  switch class(ai)
  case 'tcpip'
    % LIA data requires ai.ByteOrder = 'littleEndian'    
    numtcpchans = length(usedchans);
    navail = max(numtcpchans*8, floor(ai.BytesAvailable/sizeof(tcpdatatype)));
    numtoread = floor(min(navail, MaxPtsPerUpdate - MaxNumHeldPts - 2)/numtcpchans);
    newdata = fread(ai, numtcpchans*numtoread, tcpdatatype);
    
    %if lasttcptime > 5
    %  disp('break!');
    %end
    %disp(ai.BytesAvailable);
    
    % deinterleave
    newdata = reshape(newdata, numtcpchans, numtoread)';
    newtime = lasttcptime + (1:size(newdata,1))'/SamplesPerSecond;
    lasttcptime = newtime(end);
  case 'analoginput'
    % 12/2010: -2 to prevent error out of bounds error in DrawTraces
    navail = max(16, ai.SamplesAvailable);  % request >= 16 samps
    [newdata, newtime] = getdata(ai, min(navail, MaxPtsPerUpdate - MaxNumHeldPts - 2));
    % discard NaNs that MATLAB inserts to indicate breaks between triggers
    newdata = newdata(isfinite(newtime), :);
    newtime = newtime(isfinite(newtime));
  end % switch
  
  % update data for all channels
  len = length(newtime);
  for channame = fieldnames(channels)'
    chan = channels.(char(channame));
    % this works fine, even if cliplen > end
    % ...also, we need to prevent discard of held points
    %if displays.(chan.Display).NumHeldPts > length(chan.data) - len;
    %  cliplen = length(chan.data) - displays.(chan.Display).NumHeldPts + 1;
    %else
      cliplen = len+1;
    %end
    channels.(char(channame)).data = [chan.data(cliplen:end); newdata(:,chan.ChanIdx)]; 
  end

  allclosed = true;
  % for each plot...
  for dispname = fieldnames(displays)'
    dp = displays.(char(dispname));
    % update times for display
    displays.(char(dispname)).time = [dp.time(len+1:end); newtime];
    % use CloseRequestFcn callback for plot?    
    if ishandle(dp.haxes)
      allclosed = false;
      switch dp.Mode
      case 'Roll'
        UpdateRollDisp(char(dispname), len);
      case 'Trig'
        UpdateTrigDisp(char(dispname), len);
      end
    end
  end
  
  if allclosed
    stop_();
  end
end

% update all traces on a scrolling (strip chart) display
function UpdateRollDisp(dispname, newpts)
  dp = displays.(dispname);
  tskip = dp.tskip;
  mm = floor((newpts + dp.NumHeldPts)/tskip);
  % don't update display if not enough data for at least one pixel
  if mm > 0
    DrawTraces(dispname, 0, newpts + dp.NumHeldPts, mm*tskip, tskip);
  end
  % save remainder for next time
  displays.(dispname).NumHeldPts = (newpts + dp.NumHeldPts) - mm*tskip;
end

% Trigger holdoff: data is passed to trig function normally and state 
%  changes are recorded, but triggers within holdoff time are ignored. 
%  Consider option to prevent recording of state changes or passing data 
%  to trig fn

% check for trigger event and, if any, update all traces on a triggered display
function UpdateTrigDisp(dispname, newpts)
  disp = displays.(dispname);
  % process triggers 
  %for ii = 1:length(display.triggers)
    ii = 1;
    trigname = char(disp.triggers{ii});
    trig = triggers.(trigname);

    % NOTE: internally, delay is from trigger edge to end of display data
    % if delay < 0, pass data at -delay to trigfn to determine whether to
    %  display old data
    % if delay > 0, pass old data to trig fn to determine whether to display
    %  data at +delay
    len = newpts + trig.NumHeldPts;
    triggers.(trigname).NumHeldPts = 0;
    if trig.Delay >= 0
      trigrevstop = trig.Delay;
    else
      trigrevstop = 0;
    end
    
    % this code is still pretty ugly, but good enough for now
    newstate = trig.State;
    trigstate = trig.Fn(newstate, channels, trigrevstop + len, trigrevstop);
    while 1
      trigedge = find((trigstate ~= newstate) & trigstate, 1); 
      if isempty(trigedge)
        break;
      elseif length(trigstate) - trigedge + 1 < trig.MinWidth
        % need to wait to determine if trigger is valid; note that 
        %  trig.State does NOT change until we know trigger is valid        
        if (trigstate(end) ~= newstate) & trigstate(end)
          triggers.(trigname).NumHeldPts = length(trigstate) - find(trigstate ~= trigstate(end), 1, 'last');
        end        
        break;
      else
        antitrig = find(trigstate(trigedge:trigedge+trig.MinWidth-1) ~= trigstate(trigedge), 1);
        if isempty(antitrig)
          % valid trigger
          oldstate = newstate;     
          newstate = trigstate(trigedge);
          % alternatives: newstate > oldstate
          if oldstate <= 0 && newstate > 0
            trigtime = disp.time(trigrevstop + len - trigedge);
            if trigtime > disp.NextTrigTime
              % this should work for both delay cases - might be off by one point though
              extrapts = disp.tskip*ceil(disp.Horz*SamplesPerSecond/disp.tskip) - disp.Horz*SamplesPerSecond;
              % clear force trigger if in effect
              if(isfield(triggers.(trigname), 'oldFn_'))
                triggers.(trigname).Fn = triggers.(trigname).oldFn_;
                rmfield(triggers.(trigname), 'oldFn_');
              end              
              DrawTraces(dispname, trigrevstop + len - trigedge, trigrevstop + len - trigedge - trig.Delay + disp.Horz*SamplesPerSecond + extrapts, disp.tskip*ceil(disp.Horz*SamplesPerSecond/disp.tskip), disp.tskip);
              % record holdoff time
              displays.(dispname).NextTrigTime = trigtime + trig.Holdoff;
            end
          end % end if oldstate <=0 ...
          len = len - (trigedge+trig.MinWidth-1);
          if len < 1
            break;
          end
          % call trig fn (again, passing data after first state change)            
          trigstate = trig.Fn(newstate, channels, trigrevstop + len, trigrevstop);
        else
          trigstate = trigstate(trigedge+antitrig-1:end);
          len = len - (trigedge + antitrig - 2);
        end
      end                  
    end %while
    triggers.(trigname).State = newstate;
  %end % for each trig
end

% with a few changes, we could eliminate some of the specialized
%  spectrogram code, but don't bother for now
function DrawTraces(dispname, trigrevidx, revidx, len, tskip)
  disp = displays.(dispname);
  ndisppts = length(disp.disptime);
  % adjustment...
  revidx = revidx + 1;
  % condense new time data to plot points
  if tskip <= 1
    timepts = disp.time(end-revidx:end-revidx+len-1);   
  else
    switch disp.CondType
    case {'Simple', 'Spec', 'LogSpec'}
      timepts = disp.time(end-revidx:tskip:end-revidx+len-1); 
    case {'MinMax', 'CenterSpan'}
      % should be an integer
      mm = len/tskip;
      timepts = mean(reshape(disp.time(end-revidx:end-revidx+len-1), tskip, mm)); 
      % repeat each point twice - for min and max data points
      timepts = [timepts; timepts];
      timepts = timepts(:); 
    end
  end
  if length(timepts) > ndisppts
    % don't allow disptime to grow
    displays.(dispname).disptime = timepts(end-ndisppts+1:end);
  else
    displays.(dispname).disptime = [disp.disptime(length(timepts)+1:end); timepts];
  end
  
  % distribute data to individual channels
  meastext = {};
  for ii = 1:length(disp.channels)
    channame = char(disp.channels{ii});
    chan = channels.(channame);

    % condense data for display
    if tskip <= 1
      datapts = chan.data(end-revidx:end-revidx+len-1);
    else
      switch disp.CondType
      case 'Simple'
        datapts = chan.data(end-revidx:tskip:end-revidx+len-1); 
      case 'MinMax'
        [minpts, maxpts] = chan.CondFn(reshape(chan.data(end-revidx:end-revidx+len-1), tskip, mm)); 
        datapts = [minpts; maxpts];
        datapts = datapts(:);
      case 'CenterSpan'
        [center, span] = chan.CondFn(reshape(chan.data(end-revidx:end-revidx+len-1), tskip, mm)); 
        datapts = [center-span; center+span];
        datapts = datapts(:);
      case 'LogSpec'
        % spectrogram display /w logarithmic freq axis
        % - uses constant Q transform
        npts = disp.NPoints;  % FFT length
        cq = [];
        stop = length(chan.data)-revidx+len-1;
        while stop > length(chan.data)-revidx
          cq(:,end+1) = constQ(chan.data((stop-npts+1):stop)', chan.CQKernel);
          stop = stop - tskip;
        end
        datapts = 20*log10(abs(cq));  
      case 'Spec'
        
        % TODO: test spectrogram with triggered operation
        % TODO: investigate reassigned spectrograms
        % TODO: try imresize (or own own resampling code) to make
        %  dispdata size match number of y-axis pixels in himage ... speed
        %  up only if num pixels reduced significantly?  Do all axis
        %  clipping internally (already the case for LogSpec)?
        % TODO: log scale - learn about const. Q xfrm - how to get better
        %  resolution; should we just forget about const. Q and do log
        %  sampling of linear fft result?
        % Try: http://labrosa.ee.columbia.edu/matlab/sgram/logfsgram.m
        % TODO: musical scale for y axis/overlay
        % TODO: subplot along side showing instantaneous spectrum
        
    % LogSpec: YLim = Vert = YData = [min, max] for const. Q xfrm
    % vs.: YLim = Vert != YData = [min, max] = full range or Nyquist
    % Ticks: log (evenly spaced on log axis) vs. linear (not evenly spaced)
    
        
        % spectrogram display
        % - based on experiments, it appears image() samples matrix - no
        % smoothing or interpolation; could try imresize        
        npts = disp.NPoints;  % FFT length
        % FFT overlap = (FFT length) - tskip  
        xfft = [];
        stop = length(chan.data)-revidx+len-1;
        while stop > length(chan.data)-revidx
          xfft(:,end+1) = (2/npts)*fft(chan.data((stop-npts+1):stop) .* chan.FFTWin);
          stop = stop - tskip;
        end
        % Avoid taking the log of 0.
        xfft(xfft == 0) = 1e-17;
        % Compute magnitude, dB
        datapts = 20*log10(abs(xfft(1:npts/2,:)));        
      end % switch/case
    end % if/else

    % scale/offset or more general transformation
    if ~isempty(chan.FilterFn)
      datapts = chan.FilterFn(chan.Scale*datapts + chan.Offset);
    elseif chan.Scale ~= 1 || chan.Offset ~= 0
      datapts = chan.Scale*datapts + chan.Offset;
    end
    
    % update display data
    switch disp.CondType
    case {'Spec', 'LogSpec'}
      channels.(channame).dispdata = [chan.dispdata(:, size(datapts,2)+1:end), datapts];
      otherwise
        if length(datapts) > ndisppts
          % don't allow dispdata to grow
          channels.(channame).dispdata = datapts(end-ndisppts+1:end);
        else      
          channels.(channame).dispdata = [chan.dispdata(length(datapts)+1:end); datapts];
        end
    end % switch/case
    
    % call measurement fns 
    meastext{end+1} = chan.LegendStr;
    for jj = 1:length(chan.Measurements)
      meas = chan.Measurements(jj);
      meastext{end+1} = meas.Name;
      %measwind = newtime(end) - meas.Window; 
      % num2str is actually very slow, so use sprintf
      measresult = meas.Fn(meas.prev, channels, trigrevidx);
      meastext{end+1} = sprintf('%0.3f', measresult);
      % write back measresult
      channels.(channame).Measurements(jj).prev = measresult;
    end % for each meas
  end % for each channel

  % display legend and measurements for all channels
  if ~isempty(disp.htextbox)
    set(disp.htextbox, 'String', meastext);
  end   

  % see if it is faster to make all GUI updates together
  %  or to move lineseries update into loop above
  tmax = displays.(dispname).disptime(end);
  if ~disp.ScrollTime
    displaytime = displays.(dispname).disptime - tmax;
    tmax = 0;
  else
    displaytime = displays.(dispname).disptime;
    if disp.ScrollBack > 1
      doslider(dispname);
    else
      xlim(disp.haxes, [tmax-disp.Horz, tmax]);
    end
  end
    
  ymax = -Inf;
  ymin = Inf;
  for ii = 1:length(disp.channels)
    channame = char(disp.channels{ii});
    switch disp.CondType
    case {'Spec', 'LogSpec'}
      set(channels.(channame).himage, 'XData', displaytime,...
        'CData', channels.(channame).dispdata);
    otherwise
      set(channels.(channame).hline, 'XData', displaytime,...
        'YData', channels.(channame).dispdata);
      if disp.AutoVert ~= 0
        ymax = max(ymax, max(channels.(channame).dispdata));
        ymin = min(ymin, min(channels.(channame).dispdata));
      end
      if length(displaytime) ~= length(channels.(channame).dispdata)
        warning('XData, YData length mismatch');
      end
    end % switch/case
  end % for each channel
  
  if disp.AutoVert ~= 0
    oldylim = ylim(disp.haxes);
    yrange = (ymax - ymin) * abs(disp.AutoVert);
    % so as not to be constantly changing scale, we only update ylim if
    %  a significant change occurs
    if yrange/diff(oldylim) > 1.2 || diff(oldylim)/yrange > 1.2
      ymin = (ymin+ymax - yrange)/2;
      ymax = (ymin+ymax + yrange)/2;
      if disp.AutoVert < 0 
        % grow only
        ymin = min(ymin, oldylim(1));
        ymax = max(ymax, oldylim(2));
      end
      ylim(disp.haxes, [ymin, ymax]);
    end
  end % AutoVert
  
  % 'expose' speeds things up a bit
  if disp.ForceDraw
    drawnow expose;   
  end
end % DrawTraces

function valout = access_(valname, valin)
  if nargin == 0
    % no inputs -> return list of members
    members = who();
    valout = setdiff(members, {'this', 'valname', 'valin', 'varargin'});      
  else
    if nargin > 1
      % set value
      eval([valname ' = valin;']);
    end
    valout = eval(valname);
  end % if nargin == 0
end

this.access = @access_;
this.display = @display_;
this.channel = @channel_;
this.trigger = @trigger_;
this.forcetrigger = @forcetrigger_;
this.measure = @measure_;
this.start = @start_;
this.stop = @stop_;
this.close = @close_;
this.updatenow = @UpdateAll;  % just added for debugging
this.ai = ai;

end


% default data condense function - use one sided 
% old default CondFn (CenterSpan): @(x) deal(mean(x), std(x))
function [xmin, xmax] = onesidedstds(x)
  xmean = mean(x);
  for ii = 1:size(x,2)
    geidx = (x(:,ii) >= xmean(ii));
    leidx = (x(:,ii) <= xmean(ii));
    xmax(ii) = xmean(ii) + sqrt(mean((x(geidx,ii) - xmean(ii)).^2));
    xmin(ii) = xmean(ii) - sqrt(mean((x(leidx,ii) - xmean(ii)).^2));
  end
end

% add more types as needed
function nbytes = sizeof(typename)
  typesizes = struct('int16', 2, 'double', 8);
  nbytes = typesizes.(typename);
end

% efficient implementation for constant Q transform
function cq = constQ(x, sparKernel) % x must be a row vector
  cq = fft(x,size(sparKernel,1)) * sparKernel;
end

% returns matrix with dimension fftLen x K which will convert output of
%  an fftLen-point FFT to a length-K vector
function sparKernel = sparseKernel(lims, bins, fs, thresh)
  minFreq = lims(1); maxFreq = lims(2);  
  if nargin<4 thresh = 0.0054; end % for Hamming window
  Q = 1/(2^(1/bins)-1);
  K = ceil( bins * log2(maxFreq/minFreq) );
  fftLen = 2^nextpow2( ceil(Q*fs/minFreq) );
  tempKernel = zeros(fftLen, 1);
  sparKernel = [];
  for k = K:-1:1;
    len = ceil( Q * fs / (minFreq*2^((k-1)/bins)) );
    tempKernel(1:len) = hamming(len)/len .* exp(2*pi*i*Q*(0:len-1)'/len);
    specKernel = fft(tempKernel);
    specKernel(find(abs(specKernel)<=thresh)) = 0;
    sparKernel = sparse([specKernel sparKernel]);
  end
  sparKernel = conj(sparKernel) / fftLen;
end